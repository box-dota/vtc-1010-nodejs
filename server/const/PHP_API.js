var PHP_API = module.exports = {
	START_GAME: 'start-game.php',
	START_TOURNAMENT: 'start-tournament.php',
	END_SOLO_GAME: 'end-solo-game.php',
	CHECK_TOKEN: 'check-token.php',
	UPDATE_CCU: 'update-ccu.php',
}