var _ = require('lodash');
var TableController = require('./../TableController');
var CONST = require('./../../const/CONST');
var GameID = CONST.GameID;
var CMDClient = CONST.CMDClient;
var SocketUtils = require('./../../utils/SocketUtils');
var APIUtils = require('./../APIUtils');
var PHP_API = require('./../../const/PHP_API');

var MINUTE = 1;
var TIME_GAME = (60 * MINUTE + 5) * 1000;
var HinhDau = module.exports = TableController.$extend({
	__init__: function (gameController, master, tableID, tournament_id) {
		this.$super(gameController, master, GameID.HINHDAU, tableID, tournament_id);
		this.maxPlayers = 2;
	},
	onStartGame: function (socket, data) {
		this.$super(socket, data);

		for(var i = 0; i < this.allPlayersArray.length; i++){
			this.allPlayersArray[i].score = 0;
		}
		var params = {id1: this.allPlayersArray[0].userID, id2: this.allPlayersArray[1].userID};
		if(this.isTournament) params.tournament_id = this.tournament_id;
		APIUtils.get(PHP_API.START_GAME, params, function (result) {
			if(result.code != 1){
				this.emitToAll(socket, CMDClient.CMD_START_GAME, {success: 0, msg: 'Connect API Failed'});
				return;
			}
			console.log('Start Game countdown for Table: ', this.tableID, TIME_GAME);
			this.gameOverTimeout = setTimeout(function() {
				this.onGameOver();
			}.bind(this), TIME_GAME);

			this.currentMatchID = result.id;
			this.emitToAll(socket, CMDClient.CMD_START_GAME, {success: 1, currentMatchID: result.id});
		}.bind(this));

	},
	onUserCommand: function (socket, data) {
		this.$super(socket, data);
		var player = this.getUserBySocket(socket);
		if(player){
			player.score = data.score;
		}
		SocketUtils.broadcast(socket, this.socketChannel, CMDClient.CMD_USER_COMMAND, data);
	},
	onGameOver: function(){
		this.$super();
		if(!this.isPlaying) return;
		this.isPlaying = false;
		var player0 = this.allPlayersArray[0];
		var player1 = this.allPlayersArray[1];
		var score0 = player0.score;
		var score1 = player1.score;
		var winner = '';
		var playerWin = 0;
		if(score0 > score1) {
			winner = player0.userID;
			playerWin = 1;
			this.winnerPlayer = player0;
			this.loserPlayer = player1;
		}
		else if(score1 > score0) {
			winner = player1.userID;
			playerWin = 2;
			this.winnerPlayer = player1;
			this.loserPlayer = player0;
		}
		var msg = "It's a draw";
		if(winner) msg = 'Player ' + winner + ' win!!';
		this.emitToAll(this.allPlayersArray[0].socket, CMDClient.CMD_GAME_OVER, {winner: winner, msg: msg});

		APIUtils.get(PHP_API.END_SOLO_GAME, {
			id: this.currentMatchID, 
			player_id1: this.allPlayersArray[0].userID, 
			player_id2: this.allPlayersArray[1].userID, 
			player_win: playerWin,
			number_brick: Math.max(score0, score1),
			number_brick_lose: Math.min(score0, score1)
		}, function (result) {

		});
		this.gameOverCallback && this.gameOverCallback();
	},
	onUserLeaveTable: function (player) {
		if(!this.$super(player)) return;
		if(!this.isPlaying) return;
		this.isPlaying = false;
		// kick all player, game over
		for(var i = this.allPlayersArray.length - 1; i >= 0; i--){
			this.onUserLeaveTable(this.allPlayersArray[i]);
		}
	},
	onGameOverLeave: function(player){
		var player0 = this.allPlayersArray[0];
		var player1 = this.allPlayersArray[1];
		if(!player0||!player1) return;
		var score0 = player0.score;
		var score1 = player1.score;
		var winner = '';
		var playerWin = 0;
		
		if(player.userID == player0.userID){
			winner = player1.userID;
			playerWin = 2;
			this.winnerPlayer = player1;
			this.loserPlayer = player0;
		} else {
			winner = player0.userID;
			playerWin = 1;
			this.winnerPlayer = player0;
			this.loserPlayer = player1;			
		}
		var msg = "It's a draw";
		if(winner) msg = 'Player ' + winner + ' win!!';
		APIUtils.get(PHP_API.END_SOLO_GAME, {
			id: this.currentMatchID, 
			player_id1: this.allPlayersArray[0].userID, 
			player_id2: this.allPlayersArray[1].userID, 
			player_win: playerWin,
			number_brick: Math.max(score0, score1),
			number_brick_lose: Math.min(score0, score1)
		}, function (result) {

		});

	},
	deleteTable: function() {
		this.$super();
		clearTimeout(this.gameOverTimeout);
	}
});