var ScoopController = require('./ScoopController');
var CONST = require('./../const/CONST');
var CMDClient = CONST.CMDClient;
var _ = require('lodash');
var SocketUtils = require('./../utils/SocketUtils');
var PLAYER_STATE = require('./../const/PLAYER_STATE');
var APIUtils = require('./APIUtils');
var PHP_API = require('./../const/PHP_API');
var LogUtils = require('./../utils/LogUtils');

var TournamentController = module.exports = ScoopController.$extend({
	__init__: function (gameController, master, gameID, tournamentID) {
		this.$super(master, gameID);
		this.gameController = gameController;
		this.tournamentID = tournamentID;
		this.tournament_id = undefined;
		this.socketChannel = 'tournament-' + this.tournamentID;
		this.minPlayers = 8;
		this.maxPlayers = 8;
		this.roundIndex = 0;
		this.tournamentStartTime = Date.now();
		this.startRoundTimeout;
		this.onUserJoinTournament(master);
	},
	onUserJoinTournament: function (player) {
		this.onUserJoinScoop(player);
		player.joinTournament(this);
		player.state = PLAYER_STATE.JOIN_TOURNAMENT;
		var elapsedTime = Date.now() - this.tournamentStartTime;
		var returnData = {success: 1, gameID: this.gameID, tournamentID: this.tournamentID, time: elapsedTime};
		returnData.players = [];
		for(var userID in this.allPlayersID){
			returnData.players.push({userID: userID, isMaster: this.master.userID == userID});
		}
		SocketUtils.emit(player.socket, CMDClient.CMD_JOIN_TOURNAMENT, returnData);
		SocketUtils.broadcast(player.socket, this.socketChannel, CMDClient.CMD_JOIN_TOURNAMENT, {isOther: true, userID: player.userID});

		if(this.getNumPlayers() >= this.minPlayers){
			this.startTournament();
		}
	},
	onUserLeaveTournament: function (player, msg) {
		if(!this.allPlayersID[player.userID]) return;
		this.emitToAll(player.socket, CMDClient.CMD_LEAVE_TOURNAMENT, {userID: player.userID, msg: msg || ''});
		player.leaveTournament(this);
		player.state = PLAYER_STATE.IDLE;
		this.onUserLeaveScoop(player);
		if(this.getNumPlayers() == 0) this.deleteTournament(this);
	},
	startTournament: function () {
		if(this.getNumPlayers() < this.minPlayers) return this.startRound();
		var params = {};
		for(var i = 0; i < this.allPlayersArray.length; i++) params['id' + (i + 1)] = this.allPlayersArray[i].userID;
		APIUtils.get(PHP_API.START_TOURNAMENT, params, function(result) {
			if(result.code != 1){
				this.emitToAll(this.allPlayersArray[0].socket, CMDClient.CMD_TOURNAMENT_START, {success: 0});
				return;
			}
			this.tournament_id = result.id;
			this.emitToAll(this.allPlayersArray[0].socket, CMDClient.CMD_TOURNAMENT_START, {success: 1, tournament_id: this.tournament_id});
			this.isPlaying = true;
			this.startRound();
		}.bind(this));
	},
	startRound: function () {
		LogUtils('white' ,'Start Round: ', this.roundIndex);
		var self = this;
		var playerLength = this.getNumPlayers();
		if(playerLength < 2){
			if(this.roundIndex == 0){
				for(var i = this.allPlayersArray.length - 1; i >= 0; i--){
					this.onUserLeaveTournament(this.allPlayersArray[i], 'Not enough player!!');
				}
			}
			else{
				if(this.allPlayersArray.length == 1){
					var tournamentWinner = this.allPlayersArray[0];
					tournamentWinner.socket.emit(CMDClient.CMD_TOURNAMENT_OVER, {});
				}
			}
			return;
		}
		this.roundIndex++;
		this.allPlayersArray = _.shuffle(this.allPlayersArray);
		var numberMatch = playerLength/2 << 0;
		var matchEnded = 0;
		for(var i = 0; i < numberMatch; i++){
			var player1 = this.allPlayersArray[i * 2];
			var player2 = this.allPlayersArray[i * 2 + 1];
			var table = this.gameController.createTable(player1.socket, this.gameID, this.tournament_id);
			this.gameController.onUserJoinTable(player2.socket, {tableHashID: table.tableHashID});
			this.gameController.onStartGame(table.master.socket, {tableHashID: table.tableHashID});
			table.gameOverCallback = function () {
				matchEnded++;
				var loserPlayer = this.loserPlayer || this.allPlayersArray[_.random(1)];
				self.onUserLeaveTournament(loserPlayer, 'You lost in a tournament!');
				if(matchEnded == numberMatch){
					self.startRoundTimeout = setTimeout(function () {
						self.startRound();
					}, 5000);
				}
			}
		}
		if(playerLength%2 == 1){
			this.allPlayersArray[playerLength - 1].socket.emit(CMDClient.CMD_SERVER_MESSAGE, {msg: 'You got free win! Please wait other match!'});
		}
	},
	deleteTournament: function () {
		this.gameController.deleteTournament(this);
		clearTimeout(this.startRoundTimeout);
	}
});