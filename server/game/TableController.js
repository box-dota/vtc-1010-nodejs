var ScoopController = require('./ScoopController');
var CONST = require('./../const/CONST');
var CMDClient = CONST.CMDClient;
var ImportCryptoJS = require('./../utils/ImportCryptoJS');
var _ = require('lodash');
var SocketUtils = require('./../utils/SocketUtils');
var PLAYER_STATE = require('./../const/PLAYER_STATE');

var TableController = module.exports = ScoopController.$extend({
	__init__: function (gameController, master, gameID, tableID, tournament_id) {
		this.$super(master, gameID);
		this.tableID = tableID;
		this.gameController = gameController;
		this.tournament_id = tournament_id;
		this.isTournament = typeof tournament_id != 'undefined';
		this.socketChannel = 'table-' + this.tableID;
		this.tableHashID = ImportCryptoJS.hash(tableID + "-" + master.userID, CONST.KEY);
		this.url = CONST.host + '/?gameID=' + this.tableHashID;
		this.minPlayers = 2;
		this.maxPlayers = 4;
		this.onUserJoinTable(master);
		this.currentMatchID = 0;
		this.gameOverCallback = null;
	},
	onUserJoinTable: function(player, data){
		this.onUserJoinScoop(player);
		player.joinTable(this);
		player.state = PLAYER_STATE.LOOKING_FOR_MATCH;
		var returnData = {success: 1, gameID: this.gameID, tableHashID: this.tableHashID};
		returnData.players = [];
		for(var userID in this.allPlayersID){
			returnData.players.push({userID: userID, isMaster: this.master.userID == userID});
		}
		SocketUtils.emit(player.socket, CMDClient.CMD_USER_JOIN_TABLE, returnData);
		SocketUtils.broadcast(player.socket, this.socketChannel, CMDClient.CMD_USER_JOIN_TABLE, {isOther: true, userID: player.userID});
	},
	onUserLeaveTable: function(player){
		this.emitToAll(player.socket, CMDClient.CMD_USER_LEAVE_TABLE, {userID: player.userID});
		player.leaveTable(this);
		player.state = PLAYER_STATE.IDLE;
		this.onUserLeaveScoop(player);
		if(this.master.userID == player.userID){
			var playersID = Object.keys(this.allPlayersID);
			if(playersID.length) {
				this.master = this.allPlayersID[playersID[0]];
				SocketUtils.broadcast(player.socket, this.socketChannel, CMDClient.CMD_SET_MASTER, {userID: this.master.userID});
			}
		}
		if(this.getNumPlayers() == 0) this.deleteTable();
		return true;
	},
	onStartGame: function (socket, data) {
		if(socket.id != this.master.socket.id) return SocketUtils.emit(socket, CMDClient.CMD_START_GAME, {success: 0, msg: 'Not master??'});
		this.isPlaying = true;
		this.setAllPlayerState(PLAYER_STATE.PLAYING);
		// this.emitToAll(socket, CMDClient.CMD_START_GAME, {success: 1});
	},
	onGameOver: function() {
		this.setAllPlayerState(PLAYER_STATE.IDLE);
	},
	onUserCommand: function (socket, data) {

	},
	deleteTable: function() {
		this.gameController.deleteTable(this);
	}
});