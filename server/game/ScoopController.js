var Class = require('./../utils/Class');
var CONST = require('./../const/CONST');
var CMDClient = CONST.CMDClient;
var _ = require('lodash');
var SocketUtils = require('./../utils/SocketUtils');
var PLAYER_STATE = require('./../const/PLAYER_STATE');

var ScoopController = module.exports = Class.$extend({
	__init__: function (master, gameID) {
		this.master = master;
		this.gameID = gameID;
		this.allPlayersID = {};
		this.allPlayersArray = [];
		this.isPlaying = false;
	},
	onUserJoinScoop: function (player) {
		this.allPlayersID[player.userID] = player;
		this.allPlayersArray.push(player);
	},
	onUserLeaveScoop: function (player) {
		delete this.allPlayersID[player.userID];
		var indexOf = this.allPlayersArray.indexOf(player);
		if(indexOf != -1) this.allPlayersArray.splice(indexOf, 1);

	},
	getNumPlayers: function(){
		return Object.keys(this.allPlayersID).length;
	},
	emitToAll: function (socket, command, data) {
		SocketUtils.emit(socket, command, data);
		SocketUtils.broadcast(socket, this.socketChannel, command, data);
	},
	getUserBySocket: function (socket) {
		for(var userID in this.allPlayersID){
			var player = this.allPlayersID[userID];
			if(player.socket.id == socket.id) return player;
		}
	},
	setAllPlayerState: function(state){
		for(var i = 0; i < this.allPlayersArray.length; i++){
			this.allPlayersArray[i].state = state;
		}
	}
});