var Class = require('./../../utils/Class');
var PLAYER_STATE = require('./../../const/PLAYER_STATE');

var Player = module.exports = Class.$extend({
	__init__: function(socket, userData){
		this.socket = socket;
		this.userID = userData.userID;
		this.currentTable = [];
		this.currentTournament = [];
		this.state = PLAYER_STATE.IDLE;
	},
	joinTable: function(table){
		this.currentTable.push(table.tableID);
		this.socket.join(table.socketChannel);
	},
	leaveTable: function(table){
		var indexOf = this.currentTable.indexOf(table.tableID);
		if(indexOf != -1) this.currentTable.splice(indexOf, 1);
		this.socket.leave(table.socketChannel);
	},
	joinTournament: function(tournament) {
		this.currentTournament.push(tournament.tournamentID);
		this.socket.join(tournament.socketChannel);
	},
	leaveTournament: function (tournament) {
		var indexOf = this.currentTournament.indexOf(tournament.tournamentID);
		if(indexOf != -1) this.currentTournament.splice(indexOf, 1);
		this.socket.leave(tournament.socketChannel);
	}
});
