var LogUtils = require('./../utils/LogUtils');
var CONST = require('./../const/CONST');
var restler = require('restler');

var APIUtils = module.exports = {
	getAPIStrFromObj: function (obj) {
		var str = '';
		for(var key in obj){
			str += '&' + key + '=' + obj[key];
		}
		return str;
	},
	get: function(apiName, data, callback) {
		var apiDataStr = this.getAPIStrFromObj(data);
		var getStr = CONST.API_HOST + apiName + '?' + 'password=' + CONST.API_PASSWORD + apiDataStr;
		restler.get(getStr, {
			timeout: 10000
		}).on('complete', function(res) {
			LogUtils('magenta', '[PHP_GET]', getStr, res);
			if(res instanceof Error) return callback({code: 0});
			if(!res.code) return callback({code: 0});
			else callback(res);
		}).on('timeout', function(ms) {
			LogUtils('red', '[PHP_GET]', getStr, ' Request Timeout: ' + ms + 'ms');
			callback({code: 0});
		});
	}
}