var LogUtils = require('./../utils/LogUtils');
var Class = require('./../utils/Class');
var CONST = require('./../const/CONST');
var CMDClient = CONST.CMDClient;
var GameID = CONST.GameID;
var Player = require('./component/Player');
var HinhDau = require('./game/HinhDau');
var SocketUtils = require('./../utils/SocketUtils');
var PLAYER_STATE = require('./../const/PLAYER_STATE');
var APIUtils = require('./APIUtils');
var PHP_API = require('./../const/PHP_API');
var TournamentController = require('./TournamentController');

var GameController = module.exports = Class.$extend({
	__init__: function () {
		this.allPlayersSocket = {};
		this.allPlayersID = {};
		this.tableList = {};
		this.tableHashList = {};
		this.tournamentList = {};
		this.currentTableID = 1;
		this.currentTournamentID = 1;
		this.ccuUpdateInterval = setInterval(this.updateCCU.bind(this), 30000);
	},
	updateCCU: function () {
		var ccu = Object.keys(this.allPlayersID).length;
		APIUtils.get(PHP_API.UPDATE_CCU, {ccu: ccu}, function (result) {

		});
	},
	onUserDisconnect: function(socket){
		var player = this.getUserBySocket(socket);
		if(!player) return;
		for(var i = 0; i < player.currentTable.length; i++){
			var table = this.tableList[player.currentTable[i]];
			if(!table) continue;
			table.onUserLeaveTable(player);
		}
		for(var i = 0; i < player.currentTournament.length; i++){
			var tournament = this.tournamentList[player.currentTournament[i]];
			if(!tournament) continue;
			tournament.onUserLeaveTournament(player);
		}
		delete this.allPlayersID[player.userID];
		delete this.allPlayersSocket[socket.id];
	},
	onUserLogin: function(socket, data){
		var self = this;
		if(this.getUserBySocket(socket)) return SocketUtils.emit(socket, CMDClient.CMD_LOGIN, {success: 0, msg: 'duplicate user id!!'});
		APIUtils.get(PHP_API.CHECK_TOKEN, {access_token: data.access_token}, function(result){
			if(!result.code) return SocketUtils.emit(socket, CMDClient.CMD_LOGIN, {success: 0, msg: 'Check token error'});
			var userID = result.playerid;
			var checkOldPlayer = self.allPlayersID[userID];
			if(checkOldPlayer){
				checkOldPlayer.socket.disconnect();
				delete self.allPlayersSocket[checkOldPlayer.socket.id];
			}
			self.allPlayersSocket[socket.id] = new Player(socket, {userID: userID});
			self.allPlayersID[userID] = self.allPlayersSocket[socket.id];
			SocketUtils.emit(socket, CMDClient.CMD_LOGIN, {success: 1, userID: userID});
		});
	},
	onCreateGame: function(socket, data){
		var command = CMDClient.CMD_CREATE_GAME;
		if(!this.validateData(socket, command, data, ['gameID'])) return;
		var player = this.validatePlayer(socket, command);
		if(!player) return;	
		var table = this.createTable(socket, data.gameID);
		if(!table) return SocketUtils.emit(socket, CMDClient.CMD_CREATE_GAME, {success: 0, msg: 'Create Table Failed'});
		SocketUtils.emit(socket, CMDClient.CMD_CREATE_GAME, {success: 1, url: table.url});
	},
	onAutoPlay: function (socket, data) {
		var command = CMDClient.CMD_AUTO_PLAY;
		if(!this.validateData(socket, command, data, ['gameID'])) return;
		var player = this.validatePlayer(socket, command, true);
		if(!player) return;	
		var foundTable;
		for(var tableID in this.tableList){
			var table = this.tableList[tableID];
			if(table.gameID == data.gameID && table.getNumPlayers() < table.maxPlayers){
				foundTable = table;
				break;
			}
		}
		if(!foundTable){
			var table = this.createTable(socket, data.gameID);
			if(!table) return SocketUtils.emit(socket, CMDClient.CMD_AUTO_PLAY, {success: 0, msg: 'Create Table Failed'});
		}
		else{
			this.onUserJoinTable(socket, {tableHashID: foundTable.tableHashID});
			this.onStartGame(foundTable.master.socket, {tableHashID: foundTable.tableHashID})
		}
	},
	onStartGame: function(socket, data){
		var command = CMDClient.CMD_START_GAME;
		if(!this.validateData(socket, command, data, ['tableHashID'])) return;
		var player = this.validatePlayer(socket, command);
		if(!player) return;	
		var table = this.validateTable(socket, command, data.tableHashID);
		if(!table) return;
		if(table.getNumPlayers() < table.minPlayers) return SocketUtils.emit(socket, command, {success: 0, msg: 'Not enough player!'});
		table.onStartGame(socket, data);
	},
	onUserCommand: function(socket, data){
		var command = CMDClient.CMD_USER_COMMAND;
		if(!this.validateData(socket, command, data, ['tableHashID', 'score'])) return;
		var player = this.validatePlayer(socket, command);
		if(!player) return;	
		var table = this.validateTable(socket, command, data.tableHashID);
		if(!table) return;
		table.onUserCommand(socket, data);
	},
	onUserJoinTable: function(socket, data){
		var command = CMDClient.CMD_USER_JOIN_TABLE;
		if(!this.validateData(socket, command, data, ['tableHashID'])) return;
		var player = this.validatePlayer(socket, command);
		if(!player) return;	
		var table = this.validateTable(socket, command, data.tableHashID);
		if(!table) return;
		if(table.getNumPlayers() >= table.maxPlayers) return SocketUtils.emit(socket, command, {success: 0, msg: 'Table is full!!'});

		table.onUserJoinTable(player, data);
	},
	onUserLeaveTable: function(socket, data) {
		var command = CMDClient.CMD_USER_LEAVE_TABLE;
		if(!this.validateData(socket, command, data, ['tableHashID'])) return;
		var player = this.validatePlayer(socket, command);
		if(!player) return;	
		var table = this.validateTable(socket, command, data.tableHashID);
		if(!table) return;
		table.onGameOverLeave(player);
		table.onUserLeaveTable(player);
	},
	onUserJoinTournament: function (socket, data) {
		var command = CMDClient.CMD_JOIN_TOURNAMENT;
		var player = this.validatePlayer(socket, command, true);
		if(!player) return;

		var foundTournament;
		for(var tournamentID in this.tournamentList){
			var tournament = this.tournamentList[tournamentID];
			if(tournament.getNumPlayers() < tournament.maxPlayers && !tournament.isPlaying){
				foundTournament = tournament;
				break;
			}
		}
		if(!foundTournament){
			var tournament = this.createTournament(socket);
			if(!tournament) return SocketUtils.emit(socket, command, {success: 0, msg: 'Create Tournament Failed'});
		}
		else{
			foundTournament.onUserJoinTournament(player);
		}
	},
	onUserLeaveTournament: function (socket, data) {
		var command = CMDClient.CMD_LEAVE_TOURNAMENT;
		if(!this.validateData(socket, command, data, ['tournamentID'])) return;
		var player = this.validatePlayer(socket, command);
		if(!player) return;
		var tournament = this.validateTournament(socket, command, data.tournamentID);
		if(!tournament) return;
		tournament.onUserLeaveTournament(player);
	},
	createTournament: function (socket) {
		var tournament = new TournamentController(this, this.getUserBySocket(socket), GameID.HINHDAU, this.currentTournamentID);
		this.tournamentList[tournament.tournamentID] = tournament;
		this.currentTournamentID++;
		LogUtils('yellow', 'CREATE TOURNAMENT: ', {tournamentID: tournament.tournamentID});
		return tournament;
	},
	createTable: function(socket, gameID, tournament_id){
		var table;
		switch(gameID){
			case GameID.HINHDAU:
				table = new HinhDau(this, this.getUserBySocket(socket), this.currentTableID, tournament_id);
				break;
			default:
				return;
		}
		this.tableList[table.tableID] = table;
		this.tableHashList[table.tableHashID] = table;
		this.currentTableID++;
		LogUtils('yellow', 'CREATE TABLE: ', {tableID: table.tableID, tableHashID: table.tableHashID});
		return table;
	},
	validateData: function (socket, command, data, property) {
		var isValid = true;
		if(typeof data != 'object') isValid = false;
		if(property){
			for(var i = 0; i < property.length; i++){
				if(typeof data[property[i]] == 'undefined'){
					isValid = false;
					break;
				}
			}
		}
		if(!isValid){
			SocketUtils.emit(socket, command, {success: 0, msg: 'Invalid Data'});
			return isValid;
		}
		return isValid;
	},
	validateTournament: function (socket, command, tournamentID) {
		var tournament = this.tournamentList[tournamentID];
		if(!tournament) {
			SocketUtils.emit(socket, command, {success: 0, msg: 'Tournament not exists'});
			return false;
		}
		return tournament;
	},
	validateTable: function (socket, command, tableHashID) {
		var table = this.tableHashList[tableHashID];
		if(!table) {
			SocketUtils.emit(socket, command, {success: 0, msg: 'Table not exists'});
			return false;
		}
		return table;
	},
	validatePlayer: function (socket, command, checkIdle) {
		var player = this.getUserBySocket(socket);
		if(!player) {
			SocketUtils.emit(socket, command, {success: 0, msg: 'User not exists'});
			return false;
		}
		if(checkIdle){
			if(player.state != PLAYER_STATE.IDLE){
				SocketUtils.emit(socket, command, {success: 0, msg: 'Invalid State'});
				return false;
			}
		}
		return player;
	},
	deleteTournament: function (tournament) {
		if(!tournament) return;
		LogUtils('yellow', 'DELETE TOURNAMENT: ', {tournamentID: tournament.tournamentID});
		delete this.tournamentList[tournament.tournamentID];
	},
	deleteTable: function(table){
		if(!table) return;
		LogUtils('yellow', 'DELETE TABLE: ', {tableID: table.tableID, tableHashID: table.tableHashID});
		delete this.tableHashList[table.tableHashID];
		delete this.tableList[table.tableID];
	},
	deleteTableByHashID: function(tableHashID){
		this.deleteTable(this.tableHashList[tableHashID]);
	},
	deleteTableByID: function(tableID){
		this.deleteTable(this.tableList[tableID]);
	},
	getUserBySocket: function(socket){
		return this.allPlayersSocket[socket.id];
	},
	getUserByID: function(userID) {
		return this.allPlayersID[userID];
	}
});