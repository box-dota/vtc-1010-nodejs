var CMDClient = require('./../const/CMDClient');

var Utility = module.exports = {
	getCommandStrFromCommand: function (command) {
		for(var key in CMDClient){
			if(command == CMDClient[key]) return key;
		}
		return '';
	},
}