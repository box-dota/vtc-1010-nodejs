var CryptoJS = require('./../lib/CryptoJS');

var CryptoJSAesJson = {
    stringify: function (cipherParams) {
        var j = {ct: cipherParams.ciphertext.toString(CryptoJS.enc.Base64)};
        if (cipherParams.iv) j.iv = cipherParams.iv.toString();
        if (cipherParams.salt) j.s = cipherParams.salt.toString();
        return JSON.stringify(j);
    },
    parse: function (jsonStr) {
        var j = JSON.parse(jsonStr);
        var cipherParams = CryptoJS.lib.CipherParams.create({ciphertext: CryptoJS.enc.Base64.parse(j.ct)});
        if (j.iv) cipherParams.iv = CryptoJS.enc.Hex.parse(j.iv)
        if (j.s) cipherParams.salt = CryptoJS.enc.Hex.parse(j.s)
        return cipherParams;
    }
}

var ImportCryptoJS = module.exports = {
	encrypt: function(plain, key){
		var encryptText = CryptoJS.AES.encrypt(JSON.stringify(plain), key, {format: CryptoJSAesJson}).toString();
		return encryptText;
	},
	hash: function(plain, key){
		var encryptText = CryptoJS.AES.encrypt(JSON.stringify(plain), key, {format: CryptoJSAesJson}).toString();
		return CryptoJS.enc.Base64.parse(JSON.parse(encryptText).ct).toString();
	},
	decrypt: function(cipher, key){
		var decryptText = '';
		try{
			decryptText = JSON.parse(CryptoJS.AES.decrypt(cipher, key, {format: CryptoJSAesJson}).toString(CryptoJS.enc.Utf8));
		}
		catch(e){

		}
		return decryptText;
	}
}