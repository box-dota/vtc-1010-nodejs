var moment = require('moment');
var chalk = require('chalk');

var LogUtils = module.exports = function() {
	var logArr = Array.prototype.slice.call(arguments);
	var color = logArr[0];
	var colorText = logArr[1];
	if(!chalk[color] || !colorText) return console.log.apply(console, logArr);
	logArr.splice(0, 2);
	logArr.unshift(chalk[color]('[' + moment().format('YYYY-MM-DD HH:mm:ss.SSS') + ']' + '---' + colorText));
	console.log.apply(console, logArr);
}