var LogUtils = require('./LogUtils');
var Utility = require('./Utility');

var SocketUtils = module.exports = {
	init: function(io, gameController){
		this.io = io;
		this.gameController = gameController;
	},
	emit: function(socket, command, data){
		LogUtils('green', ('[EMIT]-' + Utility.getCommandStrFromCommand(command)), data);
		socket.emit(command, data);
	},
	broadcast: function (socket, channel, command, data) {
		var roomList = this.io.nsps['/'].adapter.rooms[channel];
		var length = roomList && Object.keys(roomList).length;
		LogUtils('green', ('[BROADCAST]-' + Utility.getCommandStrFromCommand(command)), {channel: channel, length: length}, data);
		socket.broadcast.to(channel).emit(command, data);
	},
	getUserBySocket: function () {

	}

}