var _ = require('lodash');
var LogUtils = require('./utils/LogUtils');
var CONST = require('./const/CONST');
var GameController = require('./game/GameController');
var SocketUtils = require('./utils/SocketUtils');

var CMDClient = CONST.CMDClient;
module.exports = {
	init: function (app, io) {
		this.app = app;
		this.io = io;

		this.gameController = new GameController();

		SocketUtils.init(io, this.gameController);
		this.initConnection(io);
	},
	initConnection: function (io) {
		var self = this;
		io.on('connection', function(socket){
			// console.log('onConnection' + new Date().toLocaleString());
			self.initSocket(socket);
		});
	},
	initSocket: function (socket) {
		var self = this;
		socket.on('disconnect', function(){
			self.gameController.onUserDisconnect(socket);
		});
		socket.on(CMDClient.CMD_LOGIN, function(data) {
			LogUtils('cyan', ('[ON]-CMD_LOGIN'), data);
			self.gameController.onUserLogin(socket, data);
		});
		socket.on(CMDClient.CMD_CREATE_GAME, function(data){
			LogUtils('cyan', ('[ON]-CMD_CREATE_GAME'), data);
			self.gameController.onCreateGame(socket, data);
		});
		socket.on(CMDClient.CMD_START_GAME, function(data){
			LogUtils('cyan', ('[ON]-CMD_START_GAME'), data);
			self.gameController.onStartGame(socket, data);
		});
		socket.on(CMDClient.CMD_USER_JOIN_TABLE, function(data){
			LogUtils('cyan', ('[ON]-CMD_USER_JOIN_TABLE'), data);
			self.gameController.onUserJoinTable(socket, data);
		});
		socket.on(CMDClient.CMD_USER_COMMAND, function(data){
			LogUtils('cyan', ('[ON]-CMD_USER_COMMAND'), data);
			self.gameController.onUserCommand(socket, data);
		});
		socket.on(CMDClient.CMD_AUTO_PLAY, function(data){
			LogUtils('cyan', ('[ON]-CMD_AUTO_PLAY'), data);
			self.gameController.onAutoPlay(socket, data);
		});
		socket.on(CMDClient.CMD_USER_LEAVE_TABLE, function(data){
			LogUtils('cyan', ('[ON]-CMD_USER_LEAVE_TABLE'), data);
			self.gameController.onUserLeaveTable(socket, data);
		});
		socket.on(CMDClient.CMD_JOIN_TOURNAMENT, function(data){
			LogUtils('cyan', ('[ON]-CMD_JOIN_TOURNAMENT'), data);
			self.gameController.onUserJoinTournament(socket, data);
		});
		socket.on(CMDClient.CMD_LEAVE_TOURNAMENT, function(data){
			LogUtils('cyan', ('[ON]-CMD_LEAVE_TOURNAMENT'), data);
			self.gameController.onUserLeaveTournament(socket, data);
		});
	}
};