var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var game = require('./server/game');

// constants
var PORT = 9999;

app.use(express.static(__dirname + '/client/dist'));

http.listen(PORT, function(){
	console.log('Running on http://localhost:' + PORT);
});


game.init(app, io);