console.log('we lost');
var socket = io();

var CONST = {
	gameID: 100,
	FAKE_LOGIN_URL: 'http://139.162.61.69/fake-login.php'
};
var CMDClient = {
	CMD_LOGIN: '0',
	CMD_CREATE_GAME: '1',
	CMD_START_GAME: '2',
	CMD_USER_JOIN_TABLE: '3',
	CMD_USER_DISCONNECT: '4',
	CMD_SET_MASTER: '5',
	CMD_SET_TURN: '6',
	CMD_USER_COMMAND: '7',
	CMD_GAME_OVER: '8',
	CMD_AUTO_PLAY: '9',
	CMD_USER_LEAVE_TABLE: '10',
	// CMD_USER_GET_TABLE_ID: '11',
	CMD_JOIN_TOURNAMENT: '12',
	CMD_LEAVE_TOURNAMENT: '13',
	CMD_TOURNAMENT_START: '14',
	CMD_TOURNAMENT_OVER: '15',
	CMD_SERVER_MESSAGE: '100'
}

window.onload = function () {
	initSocketListen();
	$('#username').val('welost' + (Math.random() * 1000 << 0));
	createButton('login', 'Login', function(){
		clickLogin();
	});
	createButton('autoplay', 'Play 1v1', function(){
		socket.emit(CMDClient.CMD_AUTO_PLAY, {gameID: CONST.gameID});
	});
	createButton('leaveTable', 'Leave Table', function(){
		socket.emit(CMDClient.CMD_USER_LEAVE_TABLE, {tableHashID: CONST.tableHashID});
	});
	document.body.appendChild(document.createElement('br'));
	createButton('sendCommand', 'Send +10 Score', function(){
		CONST.score += 10;
		socket.emit(CMDClient.CMD_USER_COMMAND, {tableHashID: CONST.tableHashID, score: CONST.score});
	});
	document.body.appendChild(document.createElement('br'));
	createButton('joinTournament', 'Join Tournament', function(){
		socket.emit(CMDClient.CMD_JOIN_TOURNAMENT, {});
	});
	createButton('leaveTournament', 'Leave Tournament', function(){
		socket.emit(CMDClient.CMD_LEAVE_TOURNAMENT, {tournamentID: CONST.tournamentID});
	});

	$('#login').click();
}
function clickLogin(){
	var username = $('#username').val();
	$.ajax({
		url: CONST.FAKE_LOGIN_URL,
		method: 'POST',
		data: {
			name: username,
			vtcid: Math.random() * 100000000 << 0,
			avatar_link: 'http://static.appstore.vn/a/uploads/thumbnails/122015/devo-icon-pack_icon.png'
		}
	}).done(function(data) {
		console.log('LOGIN PHP', data);
		if(!data.code) return;
		socket.emit(CMDClient.CMD_LOGIN, {access_token: data.access_token});
	});
}
function initSocketListen(){
	for(var commandStr in CMDClient){
		onServerCommand(commandStr);
	}
}
function onServerCommand(commandStr){
	var command = CMDClient[commandStr];
	socket.on(command, function (data) {
		console.log('ServerListen', commandStr, data);
		switch(command){
			case CMDClient.CMD_LOGIN:
				break;
			case CMDClient.CMD_CREATE_GAME:
				break;
			case CMDClient.CMD_START_GAME:
				CONST.score = 0;
				break;
			case CMDClient.CMD_USER_JOIN_TABLE:
				if(data.isOther){
				}
				else{
					if(data.success){
						CONST.tableHashID = data.tableHashID;
					}
				}
				break;
			case CMDClient.CMD_USER_DISCONNECT:
				break;
			case CMDClient.CMD_SET_MASTER:
				break;
			case CMDClient.CMD_SET_TURN:
				break;
			case CMDClient.CMD_USER_COMMAND:
				break;
			case CMDClient.CMD_GAME_OVER:
				break;
			case CMDClient.CMD_AUTO_PLAY:
				break;
			case CMDClient.CMD_JOIN_TOURNAMENT:
				if(data.isOther){

				}
				else{
					if(data.success){
						CONST.tournamentID = data.tournamentID;
					}
				}
				break;
		}
	})
}
function createButton(id, name, callback){
	var button = document.createElement('button');
	button.id = id;
	button.textContent = name;
	button.onclick = callback;
	document.body.appendChild(button);
}